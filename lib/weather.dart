import 'package:flutter/material.dart';

void main() {
  runApp(Weather());
}

class Weather extends StatefulWidget {
  @override
  State<Weather> createState() => _Weather();
}

class _Weather extends State<Weather> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: buildAppBarWidget(),
          body: SingleChildScrollView(
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(32),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("img/bgweather.jpg"),
                    fit: BoxFit.cover
                ),

              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children:[
                  Text('ตำบลแสนสุข', style: const TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight:  FontWeight.w200
                  ),),

                  Text('26°', textAlign: TextAlign.center,style: const TextStyle(
                      color: Colors.white,
                      fontSize: 80,
                      fontWeight:  FontWeight.w500
                  ),),
                  Text('มีเมฆเป็นส่วนใหญ่', style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight:  FontWeight.w800
                  ),),
                  const SizedBox(
                    height: 58,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Container(
                    child:  weatherActionItems(),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Container( margin: EdgeInsets.all(0),
                    height:395,
                    width:double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.blue.shade300,
                      borderRadius: BorderRadius.circular(20), //border corner radius
                      boxShadow:[
                        BoxShadow(
                          color: Colors.white.withOpacity(0), //color of shadow
                          spreadRadius: 5, //spread radius
                          blurRadius: 7, // blur radius
                          offset: Offset(0, 2), // changes position of shadow

                        ),
                      ],
                    ),
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          buildWeatherToday(),
                          buildWeatherTomorrow(),
                          buildWeather04(),
                          buildWeather05(),
                          buildWeather06(),
                          buildWeather07(),
                          buildWeather08(),
                          buildWeather09(),

                        ]
                    ),),
                ],
              ),
            ),
          )

        )
    );
  }
}
Widget buildWeatherToday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('02/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('วันนี้      ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('28', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('21', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),

      ]
  );
}

Widget buildWeatherTomorrow(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('03/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('พรุ่งนี้    ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('30', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('22', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather04(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('04/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('พุธ        ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('31', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('22', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather05(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('05/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('พฤหัส   ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('30', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('21', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather06(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('06/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('ศุกร์       ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.water_drop,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('29', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('21', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather07(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('07/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('เสาร์       ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('29', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('21', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather08(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('08/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('อาทิตย์  ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('31', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('23', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

Widget buildWeather09(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('09/01', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('จันทร์     ', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
            size: 25,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('32', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
        Text('23', textAlign: TextAlign.center,style: const TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight:  FontWeight.w800
        ),),
      ]
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.lightBlue,
    leading: Icon(Icons.arrow_back, color: Colors.black,
    ),
    //title: Text("weather"),
    actions: <Widget>[
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.menu,
            color: Colors.black,
          )
      ),
    ],
  );
}
Widget weatherActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildcloud1Button(),
      buildcloud2Button(),
      buildcloud3Button(),
      buildcloud4Button(),
      buildcloud5Button(),
      buildcloud6Button(),
    ],
  );
}

Widget buildcloud1Button() {
  return Column(
    children: <Widget>[
      Text('19.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('26°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildcloud2Button() {
  return Column(
    children: <Widget>[
      Text('20.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('25°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildcloud3Button() {
  return Column(
    children: <Widget>[
      Text('21.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('25°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildcloud4Button() {
  return Column(
    children: <Widget>[
      Text('22.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('24°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildcloud5Button() {
  return Column(
    children: <Widget>[
      Text('23.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('24°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}

Widget buildcloud6Button() {
  return Column(
    children: <Widget>[
      Text('00.00', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight:  FontWeight.w900
      ),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('23°', textAlign: TextAlign.center,style: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight:  FontWeight.w500
      ),),
    ],
  );
}
